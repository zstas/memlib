# Info

This is a simple library for memory allocation.

It has 2 public methods to use:
```
void *malloc( size_t size );
void free( void* ptr );
```

# Allocators

## We have 3 allocators here:

### Simple allocator
Just allocate and do nothing more. Additionally, it stores the length of the allocated memory.

### Free list allocator
This type of allocator marks the freed memory to reuse it later. It is useful to have this type of allocator when your application use fixed-size blocks very frequently.

### Compact allocator
Compact allocator has a greedy strategy. It tries to insert more data on every allocated page without allocating a new page.

Library chooses which allocator to use based on size (of memory we should allocate). It's just an example and it could not meet all requirements for real-life applications.

There were 2 ways of redefining malloc and free:

* Literally, export the malloc and free functions
* Use wrapping functions (we tell ld `-Wl,--wrap=malloc` and `--wrap=free`)

I chose the second way because it's more flexible.

# Compiling
To compile libary:
```
make -C memlib
```

To compile test program:
```
make -C test-program
```

To run:
```
LD_LIBRARY_PATH=./memlib ./test-program/test.o
```

To run with gdb:

```
LD_LIBRARY_PATH=./memlib gdb ./test-program/test.o
...
Reading symbols from ./test-program/test.o...
(gdb) run
```