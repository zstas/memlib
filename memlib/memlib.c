#include "memlib.h"

// Our global vars for allocators
struct page_list *compact_allocator_first;
struct free_list *free_list_allocator_first;
size_t lower_bound;
size_t middle_bound;

// Internal stuff
struct page_list *internal_allocator;
struct memlen_list *mlist;

/*
 * This is main malloc function:
 * - We use compact allocator for small objects (because it's greedy)
 * - We use free list allocator for medium sized objects - it's useful when you need to allocate and deallocate many things with the same size
 * - And we use simple allocator for really big objects - because it has pretty small overhead
 */
void* __wrap_malloc( size_t len ) {
    void *ptr = NULL;
    if( len < lower_bound ) {
        ptr = compact_allocator_malloc( len );
    } else if( len <= middle_bound ) {
        ptr = free_list_allocator_malloc( len );
    } else {
        ptr = simple_allocator_malloc( len );
    }
    if( ptr == NULL ) {
        return ptr;
    }
    struct memlen_list *el = append_memlen_list();
    el->ptr = ptr;
    el->len = len;
    return ptr;
}


void __wrap_free( void *ptr ) {
    struct memlen_list *el = search_memlen_list( ptr );
    if( el == NULL ) {
        // We doesn't allocate this memory
        return;
    }
    if( el->len < lower_bound ) {
        compact_allocator_free( ptr );
    } else if( el->len <= middle_bound ) {
        free_list_allocator_free( ptr );
    } else {
        simple_allocator_free( ptr );
    }
    delete_memlen_entry( el );
}

__attribute__((constructor)) void foo( void ) {
    mlist = NULL;

    // We can also set these parameters constantly
    lower_bound = getpagesize() / 100; // 40B
    middle_bound = getpagesize(); // 4kB

    // Initing the compact allocator
    compact_allocator_first = page_list_construtor( sizeof( struct page_list ) );

    // Initing the free list allocator
    free_list_allocator_first = free_list_constructor();

    // Simple allocator doesn't require any initialization
}

// Basic function to allocate memory aligned to memory page size
void* allocate_mempage_aligned( size_t len ) {
    // align to fit the whole pagesize
    if( len % getpagesize() != 0 ) {
        len += getpagesize() - len % getpagesize();
    }
    void *ptr = (struct page_list*)mmap( NULL, len, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0 );
    if( ptr == MAP_FAILED ) {
        return ( void* )0;
    }
    return ptr;
}

// Basic function to deallocate memory
int deallocate_mempage_aligned( void *ptr, size_t len ) {
    // in case we don't store the original length
    if( len % getpagesize() != 0 ) {
        len += getpagesize() - len % getpagesize();
    }
    return munmap( ptr, len );
}

struct memlen_list* append_memlen_list() {
    if( mlist == NULL ) {
        mlist = compact_allocator_malloc( sizeof( struct memlen_list ) );
        memset( mlist, 0, sizeof( struct memlen_list ) );
        return mlist;
    }

    struct memlen_list *el = mlist;
    while( el->next != NULL ) {
        el = el->next;
    }
    el->next = compact_allocator_malloc( sizeof( struct memlen_list ) );
    el = el->next;
    memset( el, 0, sizeof( struct memlen_list ) );
    return el;
}

struct memlen_list* search_memlen_list( void *ptr ) {
    struct memlen_list *el = mlist;
    while( el != NULL ) {
        if( el->ptr == ptr ) {
            return el;
        }
        el = el->next;
    }
    return NULL;
}

void delete_memlen_entry( struct memlen_list* el ) {
    struct memlen_list *cur = mlist;
    if( el == cur ) {
        mlist = cur->next;
        compact_allocator_free( el );
    }

    while( cur != NULL ) {
        if( cur->next == el ) {
            cur->next = el->next;
            compact_allocator_free( el );
        }
        cur = cur->next;
    }
}