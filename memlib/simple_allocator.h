struct simple_allocator_t {
    size_t len;
};

void* simple_allocator_malloc( size_t len );
void simple_allocator_free( void *ptr );