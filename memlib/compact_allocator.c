#include "memlib.h"

/*
 * This is a naive implementation of compact allocator.
 * It uses greedy allocation in all situations.
 * - It allocate first page in minimal size
 * - When you allocate, it tries to check if existing pages can fit requiered amount of bytes
 * - If not - it allocates new page, which size is aligned to size of memory page
 * - It doesn't free allocates pages at this moment
 */

extern struct page_list *compact_allocator_first;

// Allocates page and fill the info about it
struct page_list* page_list_construtor( size_t len ) {
    size_t struct_size = sizeof( struct page_list ) - 1 + sizeof( struct mem_entry ) - 1;
    size_t whole_len = len + struct_size;
    if( whole_len % getpagesize() != 0 ) {
        whole_len += getpagesize() - whole_len % getpagesize();
    }
    struct page_list* ptr = allocate_mempage_aligned( whole_len );
    ptr->next = NULL;
    ptr->page_size = whole_len;
    ptr->remain_space = whole_len - struct_size;

    return ptr;
}

void* compact_allocator_malloc( size_t len ) {
    struct page_list *page = compact_allocator_first;
    while( page->remain_space < len ) {
        if( page->next == NULL ) {
            page->next = (struct page_list*)page_list_construtor( len );
            page = page->next;
            break;
        }
        page = page->next;
    }
    struct mem_entry *memptr = ( struct mem_entry* )&page->data;
    // we pretty sure that this page can hold our data, so no additional checks are required
    while( memptr->len != 0 ) {
        memptr = (struct mem_entry*)( (char*)memptr + memptr->len + sizeof( struct mem_entry ) - 1 );
    }
    page->remain_space -= sizeof( struct mem_entry ) - 1 + len;
    memptr->len = len;
    return (void*)&memptr->data;
}

void compact_allocator_free( void *ptr ) {
    struct page_list *page = compact_allocator_first;
    while( page != NULL ) {
        if( ( ptr > ( void* )page) && ( ptr < ( ( void* )page + page->page_size ) ) ) {
            struct mem_entry *memptr = (struct mem_entry*)( ( char* )ptr - sizeof( size_t ) );
            page->remain_space += sizeof( struct mem_entry ) - 1 + memptr->len;
            memptr->len = 0;
            return;
        }
        page = page->next;
    }
    errno = ENOENT;
}
