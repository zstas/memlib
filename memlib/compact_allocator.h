// This structure keep info about size of allocated memory
struct mem_entry {
    size_t len;
    char data;
}__attribute__((__packed__));

// Structure for memory page, it keeps the pointer to the next page
struct page_list {
    struct page_list *next;
    size_t page_size;
    size_t remain_space;
    char data;
}__attribute__((__packed__));

void* compact_allocator_malloc( size_t len );
void compact_allocator_free( void *ptr );
struct page_list* page_list_construtor( size_t len );