#include "memlib.h"

/*
 * This is a naive implementation on simple allocator
 * Just allocate what's required (and store the len)
 */

void* simple_allocator_malloc( size_t len ) {
    struct simple_allocator_t *ptr = allocate_mempage_aligned( len + sizeof( struct simple_allocator_t ) );
    ptr->len = len;
    
    return (void*)ptr + sizeof( struct simple_allocator_t );
}

void simple_allocator_free( void *ptr ) {
    struct simple_allocator_t *orig = ptr - sizeof( struct simple_allocator_t );
    if( deallocate_mempage_aligned( orig, orig->len ) != 0 ) {
        errno = ENOENT;
    }
}
