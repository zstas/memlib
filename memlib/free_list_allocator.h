struct free_list {
    void *memptr;
    size_t len;
    char is_used;
    struct free_list *next;
};

void* free_list_allocator_malloc( size_t len );
void free_list_allocator_free( void *ptr );
struct free_list* free_list_constructor();