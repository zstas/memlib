#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "compact_allocator.h"
#include "free_list_allocator.h"
#include "simple_allocator.h"

// extern void* malloc( size_t len );
// extern void free( void *ptr );

struct memlen_list {
    void *ptr;
    size_t len;
    struct memlen_list *next;
};

void* allocate_mempage_aligned( size_t len );
int deallocate_mempage_aligned( void *ptr, size_t len );

struct memlen_list* append_memlen_list();
struct memlen_list* search_memlen_list( void *ptr );
void delete_memlen_entry( struct memlen_list* el );