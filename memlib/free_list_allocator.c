#include "memlib.h"

/*
 * This is a naive implementation of free list allocator
 * It just marks elements as free instead of truly freeing.
 */

extern struct free_list *free_list_allocator_first;

void init_free_list( struct free_list *el ) {
    el->next = NULL;
    el->is_used = 0;
    el->memptr = NULL;
    el->len = 0;
}

struct free_list* free_list_constructor() {
    struct free_list *el = allocate_mempage_aligned( sizeof( struct free_list ) );
    init_free_list( el );
    return el;
}

void* free_list_allocator_malloc( size_t len ) {
    struct free_list *el = free_list_allocator_first;
    while( el->is_used == 1 || el->len < len ) {
        if( el->next == NULL ) {
            el->next = allocate_mempage_aligned( sizeof( struct free_list ) );
            init_free_list( el->next );
            break;
        }
        el = el->next;
    }
    if( el->memptr == NULL ) {
        el->memptr = allocate_mempage_aligned( len );
    } else {
    }
    el->is_used = 1;
    return el->memptr;
}

void free_list_allocator_free( void *ptr ) {
    struct free_list *el = free_list_allocator_first;
    while( el->memptr != ptr && el->next != NULL ) {
        el = el->next;
    }
    if( el->memptr == ptr ) {
        el->is_used = 0;
    }
}