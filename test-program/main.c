#include "memlib.h"
#include "stdio.h"

int main( __attribute__((unused)) int argc, __attribute__((unused)) char **argv ) {
    char *buf10 = malloc( 10 );
    char *buf11 = malloc( 10 );
    char *buf12 = malloc( 10 );
    char *buf20 = malloc( 1000 );
    char *buf21 = malloc( 1000 );
    char *buf22 = malloc( 1000 );
    char *buf30 = malloc( 10000 );
    char *buf31 = malloc( 10000 );
    char *buf32 = malloc( 10000 );
    free( buf10 );
    free( buf11 );
    free( buf12 );
    free( buf20 );
    free( buf21 );
    free( buf22 );
    free( buf30 );
    free( buf31 );
    free( buf32 );
    return 0;
}